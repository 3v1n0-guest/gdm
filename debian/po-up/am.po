# Translations into the Amharic Language.
# Copyright (C) 2002 Free Software Foundation, Inc.
# This file is distributed under the same license as the gdm2 package.
# Ge'ez Frontier Foundation <locales@geez.org>, 2002.
#
#
msgid ""
msgstr ""
"Project-Id-Version: gdm2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-04-28 22:48+0200\n"
"PO-Revision-Date: 2003-01-08 19:19+EDT\n"
"Last-Translator: Ge'ez Frontier Foundation <locales@geez.org>\n"
"Language-Team: Amharic <locales@geez.org>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../default.desktop.in.h:1
#, fuzzy
msgid "System Default"
msgstr "የነበረው"

#: ../default.desktop.in.h:2
msgid "The default session on the system"
msgstr ""

#: ../patches/11_xephyr_nested.patch:747
msgid "Run gdm in a nested window"
msgstr ""

#: ../patches/11_xephyr_nested.patch:748
msgid "Don't lock the screen on the current display"
msgstr ""

#: ../patches/11_xephyr_nested.patch:1052
msgid "Disconnect"
msgstr "ግንኙነት አቋርጥ"

#: ../patches/11_xephyr_nested.patch:1056
msgid "Quit"
msgstr "ውጣ"
